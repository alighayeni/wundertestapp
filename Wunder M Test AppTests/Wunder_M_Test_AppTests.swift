//
//  Wunder_M_Test_AppTests.swift
//  Wunder M Test AppTests
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import XCTest
@testable import Wunder_M_Test_App

class Wunder_M_Test_AppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // testing web service + raw response
    func testCarFeed() {
        
        let carfeedExpectation = self.expectation(description: "Car Feed")
       
        Webservice.shared.getResultsFor(url: .carsFeed) {
            (urlResponse: BaseModel<Placemark_Cars>?, error) in
            
            if let error = error {
                XCTFail(error.localizedDescription)
                carfeedExpectation.fulfill()
            }
            
            if let urlResponse = urlResponse, let count = urlResponse.placemarks?.count {
                print("Car feed Total count: \(String(describing: urlResponse.placemarks?.count))")
                XCTAssertNotEqual(0, count)
                carfeedExpectation.fulfill()
            }
        }

        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                print("Error occured in fetch car feed. Error: \(error.localizedDescription)")
            }
        }
    }
    
    // Test the Webservice + Convert the data into ViewModel
    func testCarFeedAddThemToViewModel() {
        
        let carfeedExpectation = self.expectation(description: "Car Feed")
        Webservice.shared.getResultsFor(url: .carsFeed) {
            (urlResponse: BaseModel<Placemark_Cars>?, error) in
            
            if let error = error {
                XCTFail(error.localizedDescription)
                carfeedExpectation.fulfill()
            }
            
            if let value = urlResponse?.placemarks?.map({ return PlacemarkViewModel($0)}), value.count > 0 {
                print("Car feed Total count: \(String(describing: value.count))")
                XCTAssertEqual(urlResponse?.placemarks?.count, value.count)
                carfeedExpectation.fulfill()
            } else {
                print("Car feed is empty!")
                XCTAssertEqual(urlResponse?.placemarks?.count, 0)
                carfeedExpectation.fulfill()
            }
        }
        
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                print("Error occured in fetch car feed. Error: \(error.localizedDescription)")
            }
        }
    }
}
