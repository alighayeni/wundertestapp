//
//  MainViewController+TableView.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/30/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import UIKit

extension MainViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.carFeed.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.carFeed.value[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "carFeedCustomView", for: indexPath) as! carFeedCustomView
        cell.placeMarkViewModel = data
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.carFeed.value[indexPath.row]
        let mapViewController:CarsOnTheMap = UIStoryboard(name: "MapView", bundle: nil).instantiateViewController(withIdentifier: "CarsOnTheMap") as! CarsOnTheMap
        mapViewController.placemark = data
        self.navigationController?.pushViewController(mapViewController, animated: true)
        
        
    }
}
