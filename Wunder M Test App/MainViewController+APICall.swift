//
//  MainViewController+APICall.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/30/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import UIKit

extension MainViewController {
    
    //MARK: get the feed from URL
    func getCarFeed(url: WebServiceURL) {
        Webservice.shared.getResultsFor(url: url) {
            (urlResponse: BaseModel<Placemark_Cars>?, error) in
            
            if let error = error {
                print("Failed to fetch vehicle list:", error)
                return
            }
            
            if let value = urlResponse?.placemarks?.map({ return PlacemarkViewModel($0)}), value.count > 0 {
                self.carFeed.value = value
            } else {
                /* TODO: A custom View should be presented in this state.
                 I suggest to show a Retry btn or show a custom message
                 in order to inform the user he/she should wait little
                 loger to see the list of available cars.*/
                print("Car List is Empty!")
            }
        }
    }
    
    
}
