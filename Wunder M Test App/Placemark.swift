//
//  Placemarks.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import Foundation

//MARK:- Cars Model for Placemark
struct Placemark_Cars: Decodable {
    
    var address: String?
    var coordinates: [Double] = []
    var engineType: String?
    var exterior: String?
    var fuel: Int?
    var interior: String?
    var name: String?
    var vin: String?
    
    private enum CodingKeys: String, CodingKey {
        case address
        case coordinates
        case engineType
        case exterior
        case fuel
        case interior
        case name
        case vin
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.address = try? container.decodeIfPresent(String.self, forKey: .address)
        self.engineType = try? container.decodeIfPresent(String.self, forKey: .engineType)
        self.exterior = try? container.decodeIfPresent(String.self, forKey: .exterior)
        self.fuel = try? container.decodeIfPresent(Int.self, forKey: .fuel)
        self.interior = try? container.decodeIfPresent(String.self, forKey: .interior)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
        self.vin = try? container.decodeIfPresent(String.self, forKey: .vin)
        // handle array of Cordinates
        var tempCoordinates: [Double] = []
        var cordinateContainer = try! container.nestedUnkeyedContainer(forKey: .coordinates)
        while !cordinateContainer.isAtEnd {
            let points = try cordinateContainer.decode(Double.self)
            tempCoordinates.append(points)
        }
        self.coordinates.append(contentsOf: tempCoordinates)
    }
}




