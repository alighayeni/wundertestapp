//
//  carFeedCustomView.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import UIKit

class carFeedCustomView: UITableViewCell {

    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var engineType: UILabel!
    @IBOutlet weak var exterior: UILabel!
    @IBOutlet weak var fuel: UILabel!
    @IBOutlet weak var interior: UILabel!
    @IBOutlet weak var vin: UILabel!

    var placeMarkViewModel: PlacemarkViewModel! {
        didSet {
            self.carName.text = placeMarkViewModel.carsName
            self.address.text = placeMarkViewModel.address
            self.engineType.text = placeMarkViewModel.engineType
            self.exterior.text = placeMarkViewModel.exterior
            self.fuel.text = placeMarkViewModel.fuel
            self.interior.text = placeMarkViewModel.interior
            self.vin.text = placeMarkViewModel.vin
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
