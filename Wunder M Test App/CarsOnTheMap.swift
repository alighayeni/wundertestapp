//
//  CarsOnTheMap.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/30/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import Foundation
import GoogleMaps

class CarsOnTheMap: UIViewController, GMSMapViewDelegate{
    
    @IBOutlet weak var gMapView: GMSMapView!
  
    var placemark: PlacemarkViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map"
        
        // set the delegates
        gMapView.delegate = self
        gMapView.isTrafficEnabled = true
        gMapView.accessibilityElementsHidden = false
        
        // check the PlaceMarks
        if let placemark = placemark {
            // Set The lat and Long
            let long = placemark.coordinate[0]
            let lat = placemark.coordinate[1]
            
            //make sure the lat and long had Value greater than 0
            if long <= 0 || lat <= 0 {
                return
            }
            
            // Set the default Camera View From Top
            let camera = GMSCameraPosition.camera(withLatitude:  lat, longitude: long, zoom: 11.0)
            self.gMapView.camera = camera
            
            // show the Vehikel on the Map
            self.showTheCarOnTheMap(placemark,longitude: long, latitude: lat)
        }
    }
    
    private func showTheCarOnTheMap(_ item: PlacemarkViewModel,longitude long: Double,latitude lat: Double)-> Void{
        
        
            let marker = GMSMarker()
            // try to use a custom image
            let markerImage = UIImage(named: "Cars")!.withRenderingMode(.alwaysTemplate)
            //creating a marker view
            let markerView = UIImageView(image: markerImage)
            marker.iconView = markerView
        
           //creating a marker view
            marker.position = CLLocationCoordinate2D(latitude:  lat, longitude: long)
            
            // set The Information
            marker.title = "\(item.carsName)"
            marker.snippet = "\(item.fuel) L\n\(item.engineType)"
        
            // bind the ViewModel to the Marker
            let data = item // selected Model
            marker.userData = data
        
            //set some marker Random Rotation to make the app more natural 
            let degrees = Double.random(in: 1..<12) * 30.0
            marker.rotation = degrees
            marker.map = gMapView
            
            //comment this line if you don't wish to put a callout bubble
            gMapView.selectedMarker = marker
            
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        // clear the marker
        self.gMapView.clear()
        
        // make the map nil
        self.gMapView = nil
    }
}

extension CarsOnTheMap {
    // MARK: Click handeling section
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("User did tap at ...")
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        print("User did long press ...")
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker){
        if let value = marker.userData as? PlacemarkViewModel {
            print(value.carsName)
            /*TODO: After the User click on the info window, this function
             will be called and then we can present a new Controller for the Users*/
        }
    }
}
