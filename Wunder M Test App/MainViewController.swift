//
//  ViewController.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import UIKit
import RxSwift

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let bag = DisposeBag()

    // this the main Car Feed array.
    var carFeed = Variable<[PlacemarkViewModel]>([])
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Feed"

        // set The Table View Delegate and Data source
        tableView.delegate = self
        tableView.dataSource = self
        
        // register the the Custom cell in this Controller
        tableView.register(UINib(nibName: "carFeedCustomView", bundle: nil), forCellReuseIdentifier: "carFeedCustomView")
    
        // Set an Observable to reload the TableView after the data got Arrived.
        carFeed.asObservable().subscribe(onNext: { value in
            // Reload the TableView should Always happens on Main Thread
            DispatchQueue.main.async (execute: {
                self.tableView.reloadData()
            })
        }).disposed(by: bag)
        
        // call the URL to get the Car feed
        self.getCarFeed(url: .carsFeed)
    }

}



