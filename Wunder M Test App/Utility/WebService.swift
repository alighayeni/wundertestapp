//
//  WebService.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import UIKit

class Webservice {
    static let shared = Webservice()

    //MARK:- Get Results For Base Model With One Element
    func getResultsFor<T>(url: WebServiceURL, completion: @escaping (BaseModel<T>?, Error?) -> ()) {
        
        self.getDataFor(url) { (data, error) in
    
            if let error = error {
                print("Error occured in if-let. Error: \(error.localizedDescription)")
                completion(nil, error)
            }
            
            if let data = data {
                do {
                    let urlResponse = try JSONDecoder().decode(BaseModel<T>.self, from: data)
                    completion(urlResponse, nil)
                } catch {
                    print("Error occured in do-catch block. Error: \(error.localizedDescription)")
                    completion(nil, error)
                }
            }
            
        }
    }
    
    //MARK:- Get Data For URL
    func getDataFor(_ urlString: WebServiceURL, completion: @escaping (Data?, Error?) -> ()) {
       
        guard let url = URL(string: urlString.urlString()) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error {
                print("Error occured in if-let. Error: \(error.localizedDescription)")
                completion(nil, error)
            }
            
            if let data = data {
                completion(data, nil)
            }
        }.resume()
    }

}
