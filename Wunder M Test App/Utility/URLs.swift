//
//  URLs.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import Foundation


enum WebServiceURL: String {
    case carsFeed = "locations.json"
  
    func urlString() -> String {
        let baseURLString = "https://s3-us-west-2.amazonaws.com/wunderbucket/"
        let mainURLString = baseURLString + self.rawValue
        
        return mainURLString
    }
}

