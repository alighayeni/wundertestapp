//
//  PlacemarkViewModel.swift
//  Wunder M Test App
//
//  Created by Ali Ghayeni on 9/29/19.
//  Copyright © 2019 Ali Ghayeni. All rights reserved.
//

import Foundation

class PlacemarkViewModel {
    
    //MARK:- Properties
    private var placemarkModel: Placemark_Cars?

    //MARK:- Getters
    var carsName: String {
        get {
            return "Name: \(self.placemarkModel?.name ?? "")"
        }
    }
    
    var address: String {
        get {
            return  "Address: \(self.placemarkModel?.address ?? "")"
        }
    }
    
    var engineType: String {
        get {
            return  "Engine Type: \(self.placemarkModel?.engineType ?? "")"
        }
    }
    
    var exterior: String {
        get {
            return  "Exterior: \(self.placemarkModel?.exterior ?? "")"
        }
    }
    
    var interior: String {
        get {
            return  "Interior: \(self.placemarkModel?.interior ?? "")"
        }
    }
    
    var fuel: String {
        get {
            if let fuel = self.placemarkModel?.fuel {
                return "Fuel: \(String(describing: fuel))"
            } else {
                return "Fuel: Not Available)"
            }
        }
    }
    
    var vin: String {
        get {
            return  "VIN Code: \(self.placemarkModel?.vin ?? "")"
        }
    }
    
    var coordinate: [Double] {
        get {
            return self.placemarkModel?.coordinates ?? [Double]()
        }
    }
    
    //MARK:- Init
    init(_ placemark: Placemark_Cars? = nil) {
        if let placemark = placemark {
            self.placemarkModel = placemark
        }
    }

}
